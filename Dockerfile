FROM node:8.7
WORKDIR /root/.aws
COPY credentials .
WORKDIR /endor
COPY . .
RUN npm install
WORKDIR /endor/app
RUN npm install
RUN npm run build
WORKDIR /endor
CMD [ "node", "./server/src/index.js" ]
EXPOSE 8081
