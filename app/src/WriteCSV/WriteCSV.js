import React from "react";
import _ from "lodash";
import {StyledInput,StyledLabel,FlexBox,StyledHeader,StyledButton} from "../Styled/styled.js";
import * as api from "../api.js";
import Snackbar from "../Snackbar/Snackbar.js";

export default class WriteCSV extends React.Component {
    state = {
      file: {},
      csvId:""
    };

    handleInputChange = (e,feild) => {
      const file = e.target.files && e.target.files[0] || {};
      switch (feild) {
        case "file":
          this.setState((prevState)=>({ file: file, csvId: prevState.csvId  || file && file.name}));
          break;
        case "csvId":
          this.setState({ csvId: e.target.value });
          break;
        default:
          break;
      }
      this.setState({errors: undefined});
    };

    validate = (csvId,file)=>{
      const errors = [];
      if(!/\.csv$/.test(csvId)){
          errors.push("File id must end with *.csv .");
      }
      if( !/^(.+)(\.csv)$/.test(this.state.file.name || "") ){
        errors.push("please select valid *.csv files.");
      }
      return errors.length == 0 ? undefined : errors ;
    }

    submit = () => {
      const csvId = this.state.csvId;
      const file = this.state.file;
      const errors = this.validate(csvId,file);
      if(errors){
        this.setState({errors});
        return;
      }
      this.setState({
        errors: undefined,
        loading: true
      });
      api.postCsv(csvId,file).
      then(data=>this.setState({
        snackbarText:"File: " + csvId + "Uploaded Successfully.",
        open: true,
        error: false,
        loading: false
      }))
      .catch(data=>this.setState({
        snackbarText:JSON.stringify(data),
        open:true, error:true,
        loading: false
      }));
    }

    openFile = ()=>{
      this.fileInput.click();
    }

    render() {
      return (
        <FlexBox alignItems="center" flexDirection="column">
          <Snackbar position="bottom" open={this.state.open} facad={this.state.error ? "error": undefined} >
            {this.state.snackbarText}
          </Snackbar>
          <StyledHeader>Write CSV</StyledHeader>
          <FlexBox justifyContent="center" style={{marginBottom:25}}>
            <StyledLabel>
              CSV Id:
            </StyledLabel>
            <StyledInput
              disabled={this.state.loading}
              value={this.state.csvId}
              onChange={(e)=>this.handleInputChange(e,"csvId")}
              type="text"></StyledInput>
            <StyledInput
              disabled={this.state.loading}
              innerRef={(fileInput)=> this.fileInput = fileInput}
              style={{display:"none"}}
              onChange={(e)=>this.handleInputChange(e,"file")}
              type="file"></StyledInput>
            <StyledButton
              disable={this.state.loading}
              onClick={this.openFile}
              disabled={this.state.loading}>Upload File</StyledButton>
          </FlexBox>
          <StyledButton
            disabled={this.state.loading}
            onClick={this.submit}>Submit</StyledButton>
          {this.state.errors ? <ul style={{color:"red"}}>{this.state.errors.map(error=><li key={_.uniqueId()}>{error}</li>)}</ul> : null }
         </FlexBox>
       );
      }
    }
