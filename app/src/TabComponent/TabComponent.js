import React, {Component} from 'react';
import styled from 'styled-components';
import {FlexBox} from '../Styled/styled';

const TabHeader = (props) => {
    const TabHeaderComponent = styled.div`
        transition: background-color 350ms ease;
        font-family:Roboto;
        color:white;
        font-size:28px;
        line-height: 40px;
        height:40px;
        text-align: center;
        margin: 5px;
        background-color:#BDC2C7;
        border-radius: 5px;
        text-shadow: 0px 0px 3px #333;
        :hover{
            background-color:#04499B;
        }
        &.active{
            background-color:#04499B;
        }
    `;
    return (
        <TabHeaderComponent
            onClick={props.onClick}
            style={props.style}
            className={"tab-header " + props.className}>
            <label className="title">
                {props.title}
            </label>
        </TabHeaderComponent>
    )
};
const FloatingFlexBox = styled(FlexBox)`
  position: fixed;
  top: 0;
  left: 0;
`;

class TabComponent extends Component {

    state = {
        selectedTab: this.props.tabs[0].title
    };

    handleTabChange = (tabTitle) => {
        this.setState({selectedTab: tabTitle});
    };

    render() {
        return (
            <div>
                <FloatingFlexBox flexDirection="column" style={{width:200}}>
                    {
                        this.props.tabs.map((tab) => <TabHeader
                            onClick={() => this.handleTabChange(tab.title)}
                            className={this.state.selectedTab == tab.title ? "active" : ""}
                            key={tab.title}
                            title={tab.title}/>)
                    }
                </FloatingFlexBox>
                    {
                        this.props.tabs.find((tab) => tab.title == this.state.selectedTab).component
                    }
            </div>
        )
    }

}

export default TabComponent;
