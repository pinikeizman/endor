import React, {Component} from 'react';
import TabComponent from './TabComponent/TabComponent';
import ReadCSV from './ReadCSV/ReadCSV.js'
import WriteCSV from './WriteCSV/WriteCSV.js'

class App extends Component {
    tabs = [
        {
            title: "Read CSV",
            component: <ReadCSV/>
        },
        {
            title: "Write CSV",
            component: <WriteCSV/>

        }
    ];

    render() {
        return (
            <div className="App" style={{height:1400}}>
                <div className="HeaderContainer">
                </div>
                <div className="ContentContainer" style={{padding:20}}>
                    <TabComponent tabs={this.tabs}/>
                </div>
            </div>
        );
    }
}

// const mapStateToProps = (state, props) => ({
//     vasts: state.vasts
// });

export default App;
