const scvServiceURL = "http://localhost:8081/csv";

export const getCsv = (csvId) => {
  return new Promise(function(resolve, reject) {
    fetch(scvServiceURL + "/?id=" + csvId)
      .then(response => response.json().then(csv =>
        resolve(csv.data.split("\n").map(csvLine => csvLine.split(',')))
      ))
      .catch(err => {
        console.log(err);
      });
  });
}

export const getAllCsvMeta = () =>
 new Promise(function(resolve, reject) {
   fetch(scvServiceURL).then(response => {
      if(response.status != 200){
          response.json().then(data => reject(data))
      }else{
        response.json().then(data => resolve(data))
      }
    });
});



export const postCsv = (csvId, file) => {
  const formData = new FormData();
  formData.append('file',file);
  formData.append('csvId',csvId);

  return new Promise(function(resolve, reject) {
    fetch(scvServiceURL, {
        method: "POST",
        body: formData,
        cors:"no-cors"
      }).then(response => {
        if(response.status != 200){
            response.json().then(data => reject(data))
        }else{
          response.json().then(data => resolve(data))
        }
      });
  });
}

const makeCancelable = (promise) => {
  let hasCanceled_ = false;

  const wrappedPromise = new Promise((resolve, reject) => {
    promise.then(
      val => hasCanceled_ ? reject({isCanceled: true}) : resolve(val),
      error => hasCanceled_ ? reject({isCanceled: true}) : reject(error)
    );
  });

  return {
    promise: wrappedPromise,
    cancel() {
      hasCanceled_ = true;
    },
  };
};
