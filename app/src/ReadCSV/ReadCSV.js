import React from "react";
import _ from "lodash";
import {StyledInput,StyledLabel,FlexBox,StyledHeader,StyledButton} from "../Styled/styled.js";
import * as api from "../api.js";
import CSVTable from "../CSVTable/CSVTable.js"
import Select from 'react-select';
import styled from 'styled-components';

const StyledSelect = styled(Select)`
  height:36px;
  font-family:Roboto;
`;

export default class ReadCSV extends React.Component {
    state = {
      cache: {}
    };
    handleInputChange = (e,feild) => {
      this.setState({ csvId: e.target.value });
    };
    validate = (csvId)=>{
      const errors = [];
      if(!/^.{1,7}$/.test(csvId)){
          errors.push("CSV Id field is required.");
      }
      return errors.length == 0 ? undefined : errors ;
    }

    submit = (csvId) => {
      if(!csvId){
        this.setState({errors:["Please enter CSV Id."]});
        return;
      }

      this.setState({errors:undefined,loading:true});
      //check cache first
      if(this.state.cache[csvId]){
        this.setState({csvTableData: this.state.cache[csvId], loading: false});
        return;
      }
      // call api
      api.getCsv(csvId).then((data)=>{
          this.cancel ? null : this.setState((prevState)=>({
            csvTableData: data, loading: false, cache: {...prevState.cache,[csvId]:data}
          }));
      }).catch(err => {
          this.setState({errors:err})
      });
    }

    componentDidMount(){
      api.getAllCsvMeta().then(Versions => {
        const csvVersions = Versions.map(Version=>({label: Version.Key, value: Version.Key}));
        this.cancel ? null : this.setState({csvVersions})
      }).catch(err => {
          this.setState({errors:err})
      });
    }

    componentWillUnmount() {
      this.cancel = true;
    }

    render() {
      return (
        <FlexBox alignItems="center" flexDirection="column">
          <StyledHeader>ReadCSV</StyledHeader>
          <FlexBox justifyContent="center">
            <StyledLabel>
              Choose csv file to view:
            </StyledLabel>
            <StyledSelect onChange={(option)=>this.setState({value:option.value},()=>this.submit(option.value))}
                    options={this.state.csvVersions}
                    clearable={false}
                    value={this.state.value}
                    disable={!!this.state.csvVersions}
                    isLoading={!this.state.csvVersions || this.state.loading}
                    style={{width:350, height:36}}
                    ></StyledSelect>
        </FlexBox>
        {this.state.errors ? <ul style={{color:"red"}}>{this.state.errors.map(error=><li key={_.uniqueId()}>{error}</li>)}</ul> : null }
        {this.state.csvTableData ? <CSVTable csvTableData={this.state.csvTableData}/> : null}
        </FlexBox>
       );
      }
    }
