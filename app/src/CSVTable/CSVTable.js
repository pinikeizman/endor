import React from "react";
import _ from "lodash";
import styled from 'styled-components';


const StyledTable = styled.table`
margin-top:25px;
min-width: 500px;
font-family: Roboto;
border-collapse: collapse;

th,td{
  font-size: 14px;
  font-family: Roboto;
  line-height:28px;
  padding:0px 5px;
}
th{
  font-weight: bold;
}
td{
  font-size: 14px;
}
thead tr{
  background: #EEE;
}
tbody tr:nth-child(2n){
  background: #aed3ff4f;
}
`

const tableHeadGenerator = (csvTableHeadList) =>
  ( <tr key={ _.uniqueId() }>
  {
    csvTableHeadList.map(csvTableHeadItem =>
      <th key={ _.uniqueId() }>{csvTableHeadItem}</th>)
  }
  </tr> );
const tableBodyGenerator = (csvTableBody) =>
   csvTableBody.map( csvTableBodyLine =>
    <tr key={ _.uniqueId() }>
      {
        csvTableBodyLine.map(csvTableBodyItem => <td key={ _.uniqueId() }>{csvTableBodyItem}</td>)
      }
    </tr>
  );


const CSVTable = (props) => {
const [csvTableHeadList, ...csvTableBody] = props.csvTableData;
  return ( <StyledTable>
            <thead>
              {
                tableHeadGenerator(csvTableHeadList)
              }
            </thead>
            <tbody>
            {
              tableBodyGenerator(csvTableBody)
            }
            </tbody>
        </StyledTable> );
}

export default CSVTable;
