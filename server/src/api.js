var AWS_S3 = require('../../node_modules/aws-sdk/clients/s3');

var s3 = new AWS_S3();

const bucketName = "endor-interview";

const getObject = (objectKey) => {
  return new Promise(function(resolve, reject) {
    var params = {
      Bucket: bucketName,
      Key: objectKey
    };

    s3.getObject(params, function(err, data) {
      if (err) {
        reject(err); // an error occurred
      } else {
        resolve(data.Body.toString()); // successful response
      }
    });

  });
}

const getAllObjectsVrsions = () => {
  return new Promise(function(resolve, reject) {
    var params = {
      Bucket: bucketName
    };
    s3.listObjectVersions(params, function(err, data){
      if (err){
        reject(err);
      }
      resolve(data);
    });
  });
}

const uploadObject = (object, key) => {
  return new Promise( (resolve, reject) => {
    s3.upload({
      Bucket: bucketName,
      Key: key,
      Body: object
    },function(err, data){
      if (err){
        reject(err);
      }
      resolve(data);
    });

    });
}

module.exports = {
  getObject, getAllObjectsVrsions, uploadObject
}
