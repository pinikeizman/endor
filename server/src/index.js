const express = require('express');
const path = require("path")
const app = express();
const _ = require("lodash");
const api = require("./api.js");
const multer = require('multer');
const upload = multer();
const fileUpload = require('express-fileupload');


// lest hide the header with the server vendor and version
app.disable('x-powered-by');

//use bodyparser for json body parsing
app.use(require('body-parser').json());
app.use(function(error, req, res, next) {
  //Catch json error
  res.status(400);
  res.type("application/json");
  res.send({
    error: "Error parsing json."
  });
});
//files uploads
app.use(fileUpload());

//set port from env with default 8081
app.set('port', process.env.PORT | 8081);

//set `/public` as static content directory to serv our index.hml (landing point)
const publicDirPath = path.resolve(__dirname, "..", "..", "app", "build");
console.log("Server serv static content on dir: \n", publicDirPath);
app.use(express.static(publicDirPath));

//							*** API ***

/*
 *	Get /fetch_vast?id=<vast_id> -> fetch vast with id of <vast_id>
 */
app.get('/csv', function(req, res) {
  console.log("GET get-csv");
  const csvId = req.query.id;
  console.log("csvId\n", csvId);

  if (csvId) {
    res.status(200);
    res.type("application/json");

    api.getObject(csvId.trim())
      .then(csv => {
        res.send({
          data: csv
        })
      }).catch(err => {
        console.log(err);
        res.status(500);
        res.send([err.message]);
      });

  } else {
    api.getAllObjectsVrsions()
      .then(csv => {
        res.send(csv.Versions);
      }
  ).catch(err => {
    console.log(err);
    res.status(500);
    res.send([err.message]);
  });
}});

// post csv
app.post('/csv', (req, res) => {
  const data = req.files.file.data; // file passed from client
  const csvId = req.body.csvId; // all other values passed from the client, like name, etc..
  console.log('files:\n', data,"\ncsvId:\n", csvId);

  api.uploadObject(data, csvId)
  .then(response => {
    console.log(response);
    res.send(response);
  }).catch(err => {
    console.log(err);
    res.status(500);
    res.send([err.message]);

  });
});

//404 redirects
app.use(function(req, res) {
  const path404 = publicDirPath + '/index.html';
  console.log(path404);
  res.type('application/json');
  res.status(404);
  res.send({
    error: 'Not Found path: ' + req.originalUrl
  });
  console.log('Not Found path: ' + req.originalUrl);
});

app.listen(app.get('port'), function() {
  console.log('Express started press ctl+c to terminate')
});
