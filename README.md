# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

  * This project solves Endor's Fullstack web developer interview challenge,
    the project is build with docker so be sure that installed: `https://www.docker.com/get-docker`.

    This project includes:

      * React application which consume REST services exposed by NodeJS Express application.

        The App includes 2 pages:

          * ReadCSV page which expose the files in the bucket and display the files on select.
          * WriteCSV page which expose file upload interface.

      * NodeJS Express app, which expose 3 REST services to manipulate Amazon S3 bucket and additionally expose a public dir
        to serve the React client app.
        * Public Dic Path: `<root>/app/build` (create react app build path).
        * REST Services:

        1.

        Property | Path
         --------|-----
            path | /csv
          method | GET  
    query params | none
          RESULT | gets all object versions from bucket.

          2.

        Property | Path
         --------|-----
            path | /csv
          method | GET
    query params | id=<file id yo fetch from bucket>
          RESULT | fetch the object with the requested id from bucket.

          3.

        Property | Path
         --------|-----
          path   | csv  
          method | POST
    content-type | multipart/form-data
     form params | file=<file to upload>, csvId=<file key in bucket>
          RESULT | post an object to the bucket

    * This app build by dockerfile.
      The docker file committing the following tasks:
        * Copy the `credentials` file to the `~/.aws` folder to set up AWS sdk credentials.
        * Copy the source code to `/endor` folder.
        * Run the command `npm install` on dirs: `/endor` && `/endor/app`.
        * Run npm run build on `/endor/app` to build the React client applicaiton.
        * Run `node /endor/server/src/index.js` to start the express server application.

### How do I get set up? ###

* Clone the source code from: `https://bitbucket.org/pinikeizman/endor.git`.
* Add your AWS key to `credentials` file.
* Run `docker build -t endor . ` to build the docker locally with tag endor.
* Run `docker run -it -d -p 8081:8081 endor` to run the docker locally under `http://localhost:8081`.
* Navigate to `http://localhost:8081` with your favorite browser.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### A quick thought about scaling ###

* In the interest of honesty this project is not production ready yet, We still need to do work on the dockerfile build.
  But if we need to scale the application to an enterprise scale we will probably need to spread our servers into
  regions, then we can create a Bucket with replications in the same regions and connect them together to save network traffic.
* The React UI application can be served via CDN provider (AKAMAI etc..) to be globally distribute and to be close to client region.
